<!DOCTYPE html>
<html>
<head>
    <title>Ajax Example</title>

    <script src = "https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js">
    </script>
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <script type="text/javascript">
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    </script>

    <script>
        function getMessage(){
            $.ajax({
                type:'POST',
                url:'/getmsg',
                data:'',
                success:function(data){
                    $("#msg").html(data.msg);
                }
            });
        }
    </script>
</head>

<body>

<div id = 'msg'>This message will be replaced using Ajax.
    Click the button to replace the message.</div>

{!! Form::open(['url' => '/getmsg']) !!} <br>
{!! Form::label('name',' Name :') !!}
{!! Form::text('name') !!} <br>
{!! Form::label('email',' Email :') !!}
{!! Form::email('email') !!} <br>
{!! Form::label('password',' Password :') !!}
{!! Form::password('password') !!} <br>

{!! Form::submit('Send') !!}

{!! Form::close() !!}
@if( session()->has('success') )
    <div>
        {!! session('success') !!}
    </div>
@endif
</body>

</html>