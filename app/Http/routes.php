<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
Route::get('ajax',function(){
    return view('message');
});
Route::post('/getmsg','AjaxController@index');
Route::get('/getmsg',function(){
   $user =  DB::table('users')->select(['id', 'name', 'email','password' ,'created_at', 'updated_at'])->get();

    return $user;
});
Route::get('/',function(){
   return view('message');
});
Route::get('display','DisplayController@show');
Route::get('displayajax','DisplayController@index');