<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
class AjaxController extends Controller
{
    public function index(Request $request){
        $formdata =  $request->All();
        $users = new User();
        $users->name = $request->get('name');
        $users->email = $request->get('email');
        $users->password = $request->get('password');
        $users->setRememberToken($request->get('_token'));
        $users->save();

        return redirect('/')->with('success','Registered');
    }
}
