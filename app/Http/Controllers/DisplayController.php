<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\DB;
use Yajra\Datatables\Facades\Datatables;
use App\User;

class DisplayController extends Controller
{
    public function index()
    {
      /*  $users = DB::table('users')->select(['id', 'name', 'password', 'email', 'created_at', 'updated_at']);*/

        return Datatables::collection(User::all())->make(true);
    }
    public function show(){
        return view('display');
    }
}
